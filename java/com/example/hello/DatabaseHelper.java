package com.example.hello;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Database name and version
    private static final String DATABASE_NAME = "calcnum.db";
    private static final int DATABASE_VERSION = 1;

    // Create table statement
    private static final String CREATE_TABLE_CALCULATION_HISTORY = "CREATE TABLE calculation_history (" +
            "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "input_expression TEXT, " +
            "result TEXT);";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Create tables
        db.execSQL(CREATE_TABLE_CALCULATION_HISTORY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Handle database upgrades if needed
    }
}
